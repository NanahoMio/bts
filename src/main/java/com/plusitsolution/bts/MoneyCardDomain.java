package com.plusitsolution.bts;

public class MoneyCardDomain {
	private String firstname;
	private String lastname;
	private int money;
	
	public MoneyCardDomain(String firstname, String lastname, int money) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.money = money;
	}
	
	public MoneyCardDomain(String firstname) {
		this.firstname = firstname;
	}

	public MoneyCardDomain(int money) {
		this.money = money;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
}

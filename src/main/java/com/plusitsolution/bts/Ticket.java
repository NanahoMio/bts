package com.plusitsolution.bts;

public class Ticket {
	private nameStation firstStation;
	private nameStation lastStation;
	private Integer distance;
	private Integer price;
	private String cardID;
	
	public Ticket(nameStation firstStation, nameStation lastStation, Integer distance, Integer price, String cardID) {
		this.firstStation = firstStation;
		this.lastStation = lastStation;
		this.distance = distance;
		this.price = price;
		this.cardID = cardID;
	}
	public nameStation getFirstStation() {
		return firstStation;
	}
	public void setFirstStation(nameStation firstStation) {
		this.firstStation = firstStation;
	}
	public nameStation getLastStation() {
		return lastStation;
	}
	public void setLastStation(nameStation lastStation) {
		this.lastStation = lastStation;
	}
	public Integer getDistance() {
		return distance;
	}
	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getCardID() {
		return cardID;
	}
	public void setCardID(String cardID) {
		this.cardID = cardID;
	}
}

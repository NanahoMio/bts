package com.plusitsolution.bts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class BTS {
	
	private static Map<String, MoneyCardDomain> MONEYCARD_MAP = new HashMap<>();
	private static Map<String, Ticket> TICKET_MAP = new HashMap<>();
	
	private static int ticketCount = 100;
	
	private static int stationFirstCount = 0;
	private static int stationLastCount;
	private static int distance;
	
	public static void main(String[] args) {
		
		//register1
		MoneyCardDomain moneyCardDomain1 = new MoneyCardDomain("Patiparn", "Seedanoi", 0);
		registerMoneyCard(moneyCardDomain1);
		//register2
		MoneyCardDomain moneyCardDomain2 = new MoneyCardDomain("Pach", "Laft", 0);
		registerMoneyCard(moneyCardDomain2);
		
		//List
		getAllMoneyCard();
//		getAllTicket();
		
		
		//addmoney1
		String ID1 = "MCB101";
		int addmoney1 = 40;
		addMoneyCard(ID1, addmoney1);
		//addmoney2
		String ID2 = "MCB102";
		int addmoney2 = 100;
		addMoneyCard(ID2, addmoney2);
		
		//List
		getAllMoneyCard();
		getAllTicket();
		
		//buyTicket1
		nameStation stationFirst1 = nameStation.a;
		nameStation stationLast1 = nameStation.a;
		String I1 = "MCB101";
		buyTicket(stationFirst1, stationLast1, I1);
		//buyTicket1
		nameStation stationFirst2 = nameStation.a;
		nameStation stationLast2 = nameStation.z;
		String I2 = "MCB102";
		buyTicket(stationFirst2, stationLast2, I2);
		
		//List
		getAllMoneyCard();
		getAllTicket();
	}
	
	public static void registerMoneyCard(MoneyCardDomain moneyCardDomain) {
		int mapSize = MONEYCARD_MAP.size();
		String cardID = "MCB" + (mapSize + 101);
		MONEYCARD_MAP.put(cardID, moneyCardDomain);
		System.out.println("**Register MoneyCard**");
		System.out.println("Your cardID: " + cardID);
		System.out.println("-------------------------------------------------------------------");
		
	}
	
	public static void addMoneyCard(String ID, int addmoney) {
		//check moneyCard have in system?
		if(MONEYCARD_MAP.get(ID) == null) {
			throw new RuntimeException("Not found MoneyCard");
		}
		else {
			String firstname = (MONEYCARD_MAP.get(ID).getFirstname());
			String lastname = (MONEYCARD_MAP.get(ID).getLastname());
			
			
			MoneyCardDomain addmoneyCard = new MoneyCardDomain(firstname, lastname, addmoney);
			MONEYCARD_MAP.put(ID, addmoneyCard);
			
			int money = (MONEYCARD_MAP.get(ID).getMoney());
			System.out.println("**Add credit**");
			System.out.println("MoneyCard is " + firstname + " " + lastname + " have " + money +"฿");
			System.out.println("-------------------------------------------------------------------");
		}
		
	}
	
	public static void buyTicket(nameStation stationFirst, nameStation stationLast, String cardID) {
		//check moneyCard have in system?
		if(MONEYCARD_MAP.get(cardID) == null) {
			throw new RuntimeException("Not found MoneyCard");
		}
		else {
			String tickectID = "TBS" + ++ticketCount;
			
//			Ticket_MAP.put(tickectID, ticket);
			int i = 0;
			int x = 0;
			
			//count station
			for (nameStation myVar : nameStation.values()) {  
				if(stationFirst == myVar) {
			    	  stationFirstCount = i;
//			    	  System.out.println("Station First: " + myVar);
			    	  break;
			      }
				else if(stationFirst != myVar) {
			    	  i= i+1;
			    }
			}
			
			for (nameStation myVar : nameStation.values()) {  
				if(stationLast == myVar) {
			    	  stationLastCount = x;
//			    	  System.out.println("Station Last: " + myVar);
			    	  break;
			      }
				else if(stationLast != myVar) {
			    	  x=x+1;
			      }
			    }
			//Calculate distance
			calculateDistance(stationFirstCount, stationLastCount);
			
			//Calculate price
			Integer price = 20;
			Integer MaxPrice = 35;
			Integer totalPrice = price * distance;
			int money =MONEYCARD_MAP.get(cardID).getMoney();
			
			if (totalPrice > MaxPrice) {
				totalPrice = MaxPrice;
				
				if (money < totalPrice) {
					throw new RuntimeException("You Card don't have enough money ");
				}
				else if (money >= totalPrice) {
					Ticket ticket = new Ticket(stationFirst, stationLast, distance,  totalPrice, cardID);
					TICKET_MAP.put(tickectID, ticket);
					deductionMoneyCard(cardID, totalPrice);
					
					System.out.println("**Buy Ticket**");
					System.out.println("Customer: " + MONEYCARD_MAP.get(cardID).getFirstname() + " " + MONEYCARD_MAP.get(cardID).getLastname());
					System.out.println("Station First: " + TICKET_MAP.get(tickectID).getFirstStation() + " station");
					System.out.println("Station Last: " + TICKET_MAP.get(tickectID).getLastStation() + " station");
					System.out.println("Distance is " + TICKET_MAP.get(tickectID).getDistance() + " station");
					System.out.println("Total price is " + TICKET_MAP.get(tickectID).getPrice() + " Bath(s)");
					System.out.println("-------------------------------------------------------------------");
					System.out.println("Remaining is " + MONEYCARD_MAP.get(cardID).getMoney() + " Bath(s)");
					System.out.println("-------------------------------------------------------------------");
				}
			}
			else if (totalPrice <= MaxPrice) {
				Ticket ticket = new Ticket(stationFirst, stationLast, distance,  totalPrice, cardID);
				TICKET_MAP.put(tickectID, ticket);
				deductionMoneyCard(cardID, totalPrice);
				
				System.out.println("**Buy Ticket**");
				System.out.println("Customer: " + MONEYCARD_MAP.get(cardID).getFirstname() + " " + MONEYCARD_MAP.get(cardID).getLastname());
				System.out.println("Station First: " + TICKET_MAP.get(tickectID).getFirstStation() + " station");
				System.out.println("Station Last: " + TICKET_MAP.get(tickectID).getLastStation() + " station");
				System.out.println("Distance is " + TICKET_MAP.get(tickectID).getDistance() + " station");
				System.out.println("Total price is " + TICKET_MAP.get(tickectID).getPrice() + " Bath(s)");
				System.out.println("-------------------------------------------------------------------");
				System.out.println("Remaining is " + MONEYCARD_MAP.get(cardID).getMoney() + " Bath(s)");
				System.out.println("-------------------------------------------------------------------");
			}
		}
	}
	
	private static void deductionMoneyCard(String cardID, Integer totalPrice) {
		int money = MONEYCARD_MAP.get(cardID).getMoney();
		int moneyNow =  money - totalPrice;
		String firstname = (MONEYCARD_MAP.get(cardID).getFirstname());
		String lastname = (MONEYCARD_MAP.get(cardID).getLastname());
		
		MoneyCardDomain deductionMoney = new MoneyCardDomain(firstname, lastname, moneyNow);
		MONEYCARD_MAP.put(cardID, deductionMoney);
	}

	//Calculate distance between stationFirs and stationLast
	public static void calculateDistance(int stationFirstCount, int stationLastCount) {
		if (stationFirstCount > stationLastCount) {
			distance = stationFirstCount - stationLastCount;
		} else if (stationFirstCount < stationLastCount) {
			distance = stationLastCount - stationFirstCount;
		} else if (stationFirstCount == stationLastCount) {
			distance = 0;
		}
	}
	
	public static void getAllMoneyCard(){
		System.out.println("***List Money Card***");
		for (Entry<String, MoneyCardDomain> entry : MONEYCARD_MAP.entrySet()) {
			System.out.println(entry.getKey() + " | " + entry.getValue().getFirstname() + " | " + entry.getValue().getLastname() + " | " + entry.getValue().getMoney() + " | ");
		}
		System.out.println("-------------------------------------------------------------------");
	}
	
	public static void getAllTicket(){
		System.out.println("***List Ticket***");
		for (Entry<String, Ticket> entry : TICKET_MAP.entrySet()) {
			System.out.println(entry.getKey() + " | " + entry.getValue().getCardID() + " | " + entry.getValue().getFirstStation() + " | " + entry.getValue().getLastStation() + " | " + entry.getValue().getDistance() + " | " + entry.getValue().getPrice() + " | ");
		}
		System.out.println("-------------------------------------------------------------------");
	}
		
}
